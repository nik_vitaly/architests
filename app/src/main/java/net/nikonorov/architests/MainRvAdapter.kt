package net.nikonorov.architests

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

/**
 * Created by Vitaly Nikonorov on 13.05.2018.
 * email@nikonorov.net
 */
class MainRvAdapter: RecyclerView.Adapter<MainRvAdapter.MainRvViewHolder>() {
    private var selectedItem = 0

    private val dataList: List<RvEntity> = listOf(
            RvEntity(R.drawable.ic_action_android, "Android", "\$150000", "\$1200"),
            RvEntity(R.drawable.ic_action_backup, "Backup", "\$15000", "\$1200000"),
            RvEntity(R.drawable.ic_action_cake, "Cake", "\$1500", "\$1200"),
            RvEntity(R.drawable.ic_action_fitness, "Fitness", "\$1500000", "\$1299999900"),
            RvEntity(R.drawable.ic_action_info, "Info", "\$1500", "\$1200"),
            RvEntity(R.drawable.ic_action_umbrella, "Umbrella and extra long text with it", "\$1500", "\$1200")
    )

    fun onItemClick(position: Int) {
        val lastSelection = selectedItem
        selectedItem = position
        notifyItemChanged(lastSelection)
        notifyItemChanged(selectedItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainRvViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.rv_item, parent, false)
        return MainRvViewHolder(itemView, this)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: MainRvViewHolder, position: Int) {
        val item = dataList[position]
        holder.iconView.setImageResource(item.iconId)
        holder.captionView.text = item.mainText
        holder.prePriceView.text = item.preliminaryPrice
        holder.finalPriceView.text = item.finalPrice
        if (selectedItem == position) {
            holder.prePriceView.visibility = View.VISIBLE
            holder.itemView.setBackgroundResource(R.drawable.shadow)
        } else {
            holder.prePriceView.visibility = View.GONE
            holder.itemView.setBackgroundResource(R.drawable.rv_item_border)
        }
    }

    class MainRvViewHolder(view: View, private val itemClickListener: MainRvAdapter): RecyclerView.ViewHolder(view), View.OnClickListener{
        override fun onClick(v: View?) {
            if (adapterPosition != RecyclerView.NO_POSITION) {
                itemClickListener.onItemClick(adapterPosition)
            }
        }

        val iconView: ImageView
        val captionView: TextView
        val prePriceView: TextView
        val finalPriceView: TextView

        init {
            view.setOnClickListener(this)
            iconView = view.findViewById(R.id.icon)
            captionView = view.findViewById(R.id.caption)
            prePriceView = view.findViewById(R.id.prePrice)
            finalPriceView = view.findViewById(R.id.finalPrice)
        }
    }

}