package net.nikonorov.architests

/**
 * Created by Vitaly Nikonorov on 13.05.2018.
 * email@nikonorov.net
 */
data class RvEntity(
        val iconId: Int,
        val mainText: String,
        val preliminaryPrice: String,
        val finalPrice: String
)
